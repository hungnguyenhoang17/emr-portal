import Keycloak from 'keycloak-js';

// Setup Keycloak instance as needed
// Pass initialization options as required or leave blank to load from 'keycloak.json'
const keycloak = Keycloak({
  url: 'https://auth.dev.fpt.healthcare/auth',
  realm: 'mental-health-dev',
  clientId: 'emr-portal',
});

keycloak.onAuthSuccess = function () {
  console.log('onAuthSuccess');
};

keycloak.onAuthLogout = function () {
  console.log('onAuthLogout!!!');
  keycloak.clearToken();
};

keycloak.onActionUpdate = function () {
  console.log('onActionUpdate');
};

keycloak.onAuthError = function () {
  console.log('onAuthError');
};

keycloak.onAuthRefreshSuccess = function () {
  console.log('onAuthRefreshSuccess');
};

keycloak.onAuthRefreshError = function () {
  console.log('onAuthRefreshError');
};

keycloak.onReady = function () {
  console.log('onReady');
};

keycloak.onTokenExpired = function () {
  console.log('onTokenExpired');
};

export default keycloak;
