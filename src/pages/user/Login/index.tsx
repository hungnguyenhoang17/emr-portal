import { message, Tabs } from 'antd';
import React, { useState } from 'react';
import { LoginForm } from '@ant-design/pro-form';
import { useIntl, SelectLang, useModel } from 'umi';
import Footer from '@/components/Footer';

import styles from './index.less';

const Login: React.FC = () => {
  const [type, setType] = useState<string>('SSO');
  const { initialState } = useModel('@@initialState');

  const intl = useIntl();

  const handleSubmit = async () => {
    try {
      if (type === 'SSO') {
        initialState?.keycloak?.login({
          redirectUri: `${window.location.protocol}//${window.location.host}/welcome`,
        });
        return;
      }
    } catch (error) {
      const defaultLoginFailureMessage = intl.formatMessage({
        id: 'pages.login.failure',
        defaultMessage: '登录失败，请重试！',
      });
      message.error(defaultLoginFailureMessage);
    }
  };

  return (
    <div className={styles.container}>
      <div className={styles.lang} data-lang>
        {SelectLang && <SelectLang />}
      </div>
      <div className={styles.content}>
        <LoginForm
          logo={<img alt="logo" src="/healthcare.svg" />}
          title="Emr portal"
          subTitle={intl.formatMessage({ id: 'pages.layouts.userLayout.title' })}
          onFinish={async () => {
            await handleSubmit();
          }}
        >
          <Tabs activeKey={type} onChange={setType}>
            <Tabs.TabPane key="SSO" tab="SSO" />
          </Tabs>
        </LoginForm>
      </div>
      <Footer />
    </div>
  );
};

export default Login;
